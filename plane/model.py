class Airplane:

    def __init__(self, engines):
        self.engines = engines

    def start_engines(self):
        self.for_each_engines(lambda engine: engine.start())

    def stop_engines(self):
        self.for_each_engines(lambda engine: engine.stop())

    def adjust_engines_rpm(self, value):
        self.for_each_engines(lambda engine: engine.adjust_rpm(value))

    def for_each_engines(self, fn):
        for name, engine in self.engines.items():
            fn(engine)


class Engine:

    def __init__(self, max_value=10000):
        self.rpm_max_value = max_value
        self.state = 'Idle'
        self.rpm = 0

    def start(self):
        self.state = 'Started'
        self.rpm = 1000

    def adjust_rpm(self, value):
        self.check_rpm_value_validity(value)
        self.rpm = value

    def stop(self):
        self.state = 'Stopped'
        self.rpm = 0

    def check_rpm_value_validity(self, value):
        if type(value) != int:
            raise ValueError(f"Value ${value} must be an integer")
        if value < 0:
            raise ValueError(f"Value ${value} must be positive")
        if value > self.rpm_max_value:
            raise ValueError(f"Value ${value} must lesser than ${self.rpm_max_value}")
        if self.state != 'Started':
            raise ValueError(f"Engine is not started.")
