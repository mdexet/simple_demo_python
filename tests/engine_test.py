from plane.model import Engine

"""
Unit tests for engine
"""


def test_engine_start():
    # _given_
    engine = Engine()

    # _when__
    engine.start()

    # __then__
    assert engine.state == 'Started'
    assert engine.rpm == 1000


def test_engine_stop():
    # _given_
    engine = Engine()

    # _when__
    engine.stop()

    # __then__
    assert engine.state == 'Stopped'
    assert engine.rpm == 0


def test_engine_idle():
    # _given_
    engine = Engine()
    # __then__
    assert engine.state == 'Idle'
    assert engine.rpm == 0


def test_engine_adjust_unstarted():
    # _given_
    engine = Engine()

    # _when__
    try:
        engine.adjust_rpm(1500)
    except ValueError as ve:
        assert str(ve) == 'Engine is not started.'


def test_engine_adjust():
    # _given_
    engine = Engine()
    engine.start()

    # _when__
    engine.adjust_rpm(1500)

    # _then_
    assert engine.rpm == 1500
