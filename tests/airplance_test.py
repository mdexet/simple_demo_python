from plane import model
import pytest

@pytest.fixture
def create_plane():
    engines = {name: model.Engine() for name in ['A', 'B', 'C', 'D']}
    airplane = model.Airplane(engines)
    return airplane, engines

def test_start(create_plane):
    airplane, _ = create_plane

    #_when_
    airplane.start_engines()

    #_then _
    assert all(engine.state == 'Started' for engine in airplane.engines.values())

def test_stop(create_plane):
    airplane, _ = create_plane

    #_when_
    airplane.stop_engines()

    #_then _
    assert all(engine.state == 'Stopped' for engine in airplane.engines.values())
